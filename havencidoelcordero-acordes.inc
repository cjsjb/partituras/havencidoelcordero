\context ChordNames
	\chords {
		\set majorSevenSymbol = \markup { "maj7" }
		\set chordChanges = ##t

		% intro
		g2. e2.:m c2. d2.

		% toda la tierra...		
		g2. e2.:m c2. d2.
		g2. e2.:m c2. d2.

		%
		a2.:m b2.:m a2.:m b2.:m
		a2.:m b2.:m c2. d2.
		d2.

		% aleluya...
		g2. e2.:m c2. d2.
		g2. e2.:m c2. d2.
		d2.

		% aleluya...
		g2. e2.:m c2. d2.
		g2. e2.:m c2. d2.
		d2.

		% outro...
		g2. e2.:m c4. d4.
		g2.
	}
