\context Staff = "Cello" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Cello"
	\set Staff.shortInstrumentName = "Cl."
	\set Staff.midiInstrument = "Cello"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-cello" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 6/8
		\clef "bass"

                \key g \major
                g 2 fis 8 g  |
                e 2 d 8 e  |
                c 2 b, 8 c  |
                d 2.  |
%% 5
                R2.*4  |
                g 2 fis 8 g  |
%% 10
                e 2 d 8 e  |
                c 2 b, 8 c  |
                d 2.  |
                a, 2.  |
                b, 2.  |
%% 15
                c 2.  |
                b, 2.  |
                a, 2.  |
                b, 2.  |
                c 2.  |
%% 20
                d 2.  |
                d 2 e 8 fis  |
                g 2 fis 8 g  |
                e 2 d 8 e  |
                c 2 b, 8 c  |
%% 25
                d 2 e 8 fis  |
                g 2 fis 8 g  |
                e 2 d 8 e  |
                c 2 b, 8 c  |
                d 2.  |
%% 30
                d 2 e 8 fis  |
                g 2 fis 8 g  |
                e 2 d 8 e  |
                c 2 b, 8 c  |
                d 2 e 8 fis  |
%% 35
                g 2 fis 8 g  |
                e 2 d 8 e  |
                c 2 b, 8 c  |
                d 2.  |
                d 2 e 8 fis  |
%% 40
                g 2 fis 8 g  |
                e 2 d 8 e  |
                c 8 c 4 -\staccato d 8 d 4 -\staccato  |
                g 2.  |
                R2.  |
		\bar "|."
	}
>>
