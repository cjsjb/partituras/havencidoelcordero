\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble_8"
		\key g \major

		R2.*4  |
%% 5
		d 8. d 8 d 16 d 8 e fis  |
		g 8. g 8 g 16 g 8. r16 g 8  |
		g 8. g 8 g 16 g 8 a b  |
		a 2 ~ a 8 r  |
		d 8. d 8 d 16 d 8 e fis  |
%% 10
		g 8. g 8 g 16 g 8 g g  |
		g 8. g 8 g 16 g 8 a b  |
		a 2 ~ a 8 r  |
		e 8. a 8 a 16 a 8 b c'  |
		b 8. b 8 a 16 b 8. r16 e 8  |
%% 15
		e 8. a 8 a 16 a 8 b c'  |
		b 2 r8 e  |
		e 8. a 8 a 16 a 8 b c'  |
		b 8. b 8 a 16 b 8. r16 b 8  |
		c' 8. c' 8 c' 16 c' 8 b c'  |
%% 20
		d' 2. ~  |
		d' 4. r8 b c'  |
		d' 8. g r8 b c'  |
		d' 8. g r8 a b  |
		c' 8. c' 8 c' 16 c' 8 b g  |
%% 25
		a 4. r8 b c'  |
		d' 8. g r8 b c'  |
		d' 8. g r8 a b  |
		c' 8. c' 8 c' 16 c' 8 b g  |
		a 4. ( b  |
%% 30
		c' 4. ) r8 b c'  |
		d' 8. g r8 b c'  |
		d' 8. g r8 a b  |
		c' 8. c' 8 c' 16 c' 8 b g  |
		a 4. r8 b c'  |
%% 35
		d' 8. g r8 b c'  |
		d' 8. g r8 a b  |
		c' 8. c' 8 c' 16 c' 8 b g  |
		a 4. ( b  |
		c' 4. ) r8 b ( a  |
%% 40
		g 2. )  |
		R2.*4  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-tenor" {
		To -- da la tie -- rra "te a" -- la -- be, Se -- ñor,
		"Te a" -- la -- ben la lu -- na "y el" sol. __
		To -- da la tie -- rra "te a" -- la -- be, Se -- ñor,
		las es -- tre -- llas te rin -- dan lo -- or. __

		To -- dos los re -- yes del mun -- do, Se -- ñor,
		se pos -- tren an -- te "tu es" -- plen -- dor,
		e -- xul -- ten el cie -- lo, la tie -- rra "y el" mar,
		"y es" -- ta -- llen en u -- na can -- ción: __

		¡A -- le -- lu -- ya, a -- le -- lu -- ya,
		ha lle -- ga -- do "ya el" rei -- no de Dios!
		¡Ha ven -- ci -- do el Cor -- de -- ro,
		ha triun -- fa -- do con ar -- mas "de a" -- mor! __

		¡A -- le -- lu -- ya, a -- le -- lu -- ya,
		ha lle -- ga -- do "ya el" rei -- no de Dios!
		¡Ha ven -- ci -- do el Cor -- de -- ro,
		ha triun -- fa -- do con ar -- mas "de a" -- mor! __
		Oh. __
	}
>>
