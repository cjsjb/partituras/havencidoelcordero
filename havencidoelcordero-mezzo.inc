\context Staff = "mezzo" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-mezzo" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble"
		\key g \major

		R2.*4  |
%% 5
		d' 8. d' 8 d' 16 d' 8 e' fis'  |
		g' 8. g' 8 g' 16 g' 8. r16 g' 8  |
		e' 8. e' 8 e' 16 e' 8 fis' g'  |
		fis' 2 ~ fis' 8 r  |
		d' 8. d' 8 d' 16 d' 8 e' fis'  |
%% 10
		g' 8. g' 8 g' 16 g' 8 g' g'  |
		e' 8. e' 8 e' 16 e' 8 fis' g'  |
		fis' 2 ~ fis' 8 r  |
		e' 8. a' 8 a' 16 a' 8 g' a'  |
		fis' 8. fis' 8 e' 16 fis' 8. r16 e' 8  |
%% 15
		e' 8. a' 8 a' 16 a' 8 g' a'  |
		fis' 2 r8 e'  |
		e' 8. a' 8 a' 16 a' 8 g' a'  |
		fis' 8. fis' 8 e' 16 fis' 8. r16 fis' 8  |
		g' 8. g' 8 g' 16 g' 8 fis' g'  |
%% 20
		fis' 2. ~  |
		fis' 4. r8 d' d'  |
		g' 8. g' r8 g' fis'  |
		e' 8. e' r8 e' e'  |
		c' 8. c' 8 c' 16 c' 8 d' e'  |
%% 25
		d' 4. r8 d' d'  |
		g' 8. g' r8 g' fis'  |
		e' 8. e' r8 e' e'  |
		c' 8. c' 8 c' 16 c' 8 d' e'  |
		d' 4. ( e'  |
%% 30
		fis' 4. ) r8 d' d'  |
		g' 8. g' r8 g' fis'  |
		e' 8. e' r8 e' e'  |
		c' 8. c' 8 c' 16 c' 8 d' e'  |
		d' 4. r8 d' d'  |
%% 35
		g' 8. g' r8 g' fis'  |
		e' 8. e' r8 e' e'  |
		c' 8. c' 8 c' 16 c' 8 d' e'  |
		d' 4. ( e'  |
		fis' 4. ) r8 g' ( fis'  |
%% 40
		d' 2. )  |
		R2.*4  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-mezzo" {
		To -- da la tie -- rra "te a" -- la -- be, Se -- ñor,
		"Te a" -- la -- ben la lu -- na "y el" sol. __
		To -- da la tie -- rra "te a" -- la -- be, Se -- ñor,
		las es -- tre -- llas te rin -- dan lo -- or. __

		To -- dos los re -- yes del mun -- do, Se -- ñor,
		se pos -- tren an -- te "tu es" -- plen -- dor,
		e -- xul -- ten el cie -- lo, la tie -- rra "y el" mar,
		"y es" -- ta -- llen en u -- na can -- ción: __

		¡A -- le -- lu -- ya, a -- le -- lu -- ya,
		ha lle -- ga -- do "ya el" rei -- no de Dios!
		¡Ha ven -- ci -- do el Cor -- de -- ro,
		ha triun -- fa -- do con ar -- mas "de a" -- mor! __

		¡A -- le -- lu -- ya, a -- le -- lu -- ya,
		ha lle -- ga -- do "ya el" rei -- no de Dios!
		¡Ha ven -- ci -- do el Cor -- de -- ro,
		ha triun -- fa -- do con ar -- mas "de a" -- mor! __
		Oh. __
	}
>>
